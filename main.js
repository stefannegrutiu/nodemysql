var express = require('express');
var app = express();
var path = require('path');

var mysql = require('mysql');

var myConnection  = require('express-myconnection')

var dbOptions = {
  host: 'localhost', 
  user: 'root',
  password: 'password',
  port: 3306, 
  database: 'personsdb'
}

app.use(myConnection(mysql, dbOptions, 'pool'))

app.set('view engine', 'html')

app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

var routes = require('./routes')


var bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/', routes)

app.listen(3000, function(){
  console.log('Server running at port 3000: http://127.0.0.1:3000')
});

