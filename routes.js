var express = require('express')
var app = express()
 
app.get('/', function(req, res) {
  res.render('index', {data: []});
});

app.get('/stages', function(req, res) {
  req.getConnection(function(error, conn) {
    const query = "SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'persons' AND COLUMN_NAME = 'stage'";
    conn.query(query, function(err, rows) {
      if (err) res.status(400);
      const enumVal = rows[0].COLUMN_TYPE;
      const regExp = /\(([^)]+)\)/;
      const matches = regExp.exec(enumVal);
      const enumArray = matches[1].replace(/\'/g, '').split(',');
      res.status(200).json(enumArray);
    })
  }) 
});


app.get('/persons', function(req, res) {
  req.getConnection(function(error, conn) {
    const query = "SELECT * FROM persons ORDER BY name ASC";
    conn.query(query, function(err, rows) {
      if (err) res.status(400);
      res.status(200).json({rows});
    })
  })  
});

app.post('/persons', function(req, res){    
  var person = {
    name: req.body.name,
    weight: req.body.weight,
    date: new Date(req.body.date),
    stage: req.body.stage,
  }
  req.getConnection(function(error, conn) {
    const query = 'INSERT INTO persons SET ?';
    conn.query(query, person, function(err, result) {
        if (err) res.status(400);
        res.status(200).json({id: result.insertId});
    });
  });
});

app.put('/persons', function(req, res){    
  var person = {
    id: req.body.id,
    name: req.body.name,
    weight: req.body.weight,
    date: new Date(req.body.date),
    stage: req.body.stage,
  }
  req.getConnection(function(error, conn) {
    const query = `UPDATE persons SET ? WHERE id = ${person.id}`;
    conn.query(query, person, function(err) {
        if (err) res.status(400);           
        res.status(200).json({});
    });
  });
});

app.delete('/persons/:id', function(req, res) {
  var personId = req.params.id;
  req.getConnection(function(error, conn) {
    const query = `DELETE FROM persons WHERE id = ${personId}`;
    conn.query(query, function(err) {
        if (err)
          res.status(400);
        res.status(200).json({});
    });
  });
});

module.exports = app;